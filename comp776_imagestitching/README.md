## Introduction

Image stitching exercise from [COMP 776 Spring 2010](http://www.cs.unc.edu/~lazebnik/spring10/assignment3.html)  

This exercise explores on the image stitching problem and attempts to solve it using feature matching and homography identification.  

The implentation used Python 3 and OpenCV 3.

## Program Description

**image_stitcher.py** takes in two images, the left and the right image in the images/ folder, and outputs the stitched image.  

Usage example:

```
python image_stitcher.py -l image_left.jpg -r image_right.jpg
```

## Implementation
