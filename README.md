Introduction
-----
Computer vision and image processing exercises I've done or currently doing.

Links
-----
- CS4495 [spreadsheet](https://docs.google.com/spreadsheets/d/1ecUGIyhYOfQPi3HPXb-7NndrLgpX_zgkwsqzfqHPaus/pubhtml) containing slides and problem sets on different computer vision areas. I learned a lot from their online course. If you have time, please check it out [here](https://www.cc.gatech.edu/~afb/classes/CS4495-Spring2015-OMS/). It's **free**!
- COMP776 [Homework](http://www.cs.unc.edu/~lazebnik/spring10/assignment3.html).