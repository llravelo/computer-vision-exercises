"""
Image Processing Exercise
Basic Green Screen
"""

import argparse
import os
import cv2
import numpy as np


parser = argparse.ArgumentParser(description="Merges a greenscreen image with a background")
parser.add_argument('-p', action="store", dest="portrait", default="../images/girl.jpg",
                    help="Specify portrait (image with greenscreen) source")
parser.add_argument('-b', action="store", dest="background", default="../images/beach.jpg",
                    help="Specify background image source")
parser.add_argument('-t', action="store", dest="thresh", default=220, type=int,
                    help="Set greenscreen threshold")

args = parser.parse_args()

if not os.path.isfile(args.portrait) or not os.path.isfile(args.background):
    print('Cannot find images!')
    exit()

portrait = cv2.imread(args.portrait, cv2.IMREAD_COLOR)
background = cv2.imread(args.background, cv2.IMREAD_COLOR)

# Generate mask for green screen and background
# Channels: B=0 G=1 R=2
ret, mask_bg = cv2.threshold(portrait[:,:,1], args.thresh, 255, cv2.THRESH_BINARY)
mask_p = cv2.bitwise_not(mask_bg)

# Merge into three channels for bitwise operation
mask_p = cv2.merge((mask_p, mask_p, mask_p))
mask_bg = cv2.merge((mask_bg, mask_bg, mask_bg))

p = cv2.bitwise_and(portrait, mask_p)
bg = cv2.bitwise_and(background, mask_bg)

# Add two masked images
res = cv2.add(p, bg)

cv2.namedWindow('Green Screen', cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)
cv2.imshow('Green Screen', res)
cv2.waitKey(0)

cv2.imwrite('../output/greenscreen.jpg', res)
cv2.destroyAllWindows()