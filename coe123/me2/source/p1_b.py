"""
ME 2 Part 1: Digital Image Generation and Resolution
Task 2: Generate Gaussian Image with gray level 255
and similar to figure 3 in pdf
"""

import argparse
import cv2
import numpy as np

import gaussianimage as gi


parser = argparse.ArgumentParser(description='Generates Gaussian Grayscale Image')
parser.add_argument('-l', action="store", dest="side_length", type=int, default=256,
                    help="Set image dimension")
parser.add_argument('-k', action="store", dest="gray_level", type=int, default=255,
                    help="Set maximum gray level")
parser.add_argument('-s', action="store", dest="sigma", type=int, default=35,
                    help="Set stddev of 2d gaussian function")
parser.add_argument('-f', action="store", dest="filename", default="gaussianimage.jpg",
                    help="Set filename")

args = parser.parse_args()

gaussian_image = gi.generateGaussianImage(sidelength=args.side_length,
                                          sigma=args.sigma,
                                          graylevel = args.gray_level)

cv2.imwrite('../output/%s' % (args.filename), gaussian_image)

cv2.namedWindow('Gaussian Image', cv2.WINDOW_NORMAL
                                | cv2.WINDOW_AUTOSIZE)
cv2.imshow('Gaussian Image', gaussian_image)
cv2.waitKey(0)
cv2.destroyAllWindows()
