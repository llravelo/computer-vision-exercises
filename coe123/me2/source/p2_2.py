"""
Image Processing Exercise
Threshold
"""

import argparse
import os
import cv2
import numpy as np


def nothing(x):
    pass

cv2.namedWindow('Threshold', cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)
cv2.createTrackbar('Value', 'Threshold', 127, 255, nothing)

parser = argparse.ArgumentParser(description="Image Trackbar Threshold")
parser.add_argument('--im1_src', action="store", dest="im1", default="../images/image1.tif")
parser.add_argument('--im2_src', action="store", dest="im2", default="../images/image2.tif")

args = parser.parse_args()

if not os.path.isfile(args.im1) or not os.path.isfile(args.im2):
    print('Invalid image paths!')
    exit()

image1 = cv2.imread(args.im1, cv2.IMREAD_GRAYSCALE)
image2 = cv2.imread(args.im2, cv2.IMREAD_GRAYSCALE)

if not image1.shape == image2.shape:
    print('Image 1 and 2 must be the same size!')
    exit()

new_image = np.append(image1, image2, axis=0)
old_thresh = 0

while True:
    key = cv2.waitKey(30) & 0xFF
    if key == ord('q'):
        break

    threshold = cv2.getTrackbarPos('Value', 'Threshold')
    if threshold == old_thresh:
        continue

    ret, im1_binary = cv2.threshold(image1, threshold, 255, cv2.THRESH_BINARY)
    ret, im2_binary = cv2.threshold(image2, threshold, 255, cv2.THRESH_BINARY)
    new_binary = np.append(im1_binary, im2_binary, axis=0)
    fin = np.append(new_image, new_binary, axis=1)
    cv2.imshow('Threshold', fin)
    old_thresh = threshold

cv2.destroyAllWindows()
