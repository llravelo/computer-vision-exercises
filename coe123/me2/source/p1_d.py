"""
ME 2 Part 1: Digital Image Generation and Resolution
Task 2: Generate Gaussian Image with constant gray
level to stddev ratio
"""

import argparse
import cv2
import numpy as np
from matplotlib import pyplot as plt

import gaussianimage as gi


parser = argparse.ArgumentParser(description="Checkboard effect demo")
parser.add_argument('--ratio', action="store", dest="ratio", type=int, default=10,
                    help="Set side length to stddev ratio")

args = parser.parse_args()

side_length = [16, 32, 64, 128, 256, 512]
gl = 256
sigma = [i/args.ratio for i in side_length]

im = []
plotnum = [i for i in range(231, 231 + len(side_length))]  # 3-digit: nrows ncols plotnum

for i in range(len(side_length)):
    image = gi.generateGaussianImage(sidelength=side_length[i], graylevel=gl, sigma=sigma[i])
    im.append(image)

for i in range(len(side_length)):
    plt.subplot(plotnum[i])
    plt.imshow(im[i], cmap='gray')
    plt.xticks([]), plt.yticks([])
    plt.title('Side Length %d' % (side_length[i]))

plt.savefig('../output/checkerboard.jpg')
plt.show()