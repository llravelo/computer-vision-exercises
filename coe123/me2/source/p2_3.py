"""
Image Processing Exercise
Bit Place Slicing a Grayscale Image
"""

import argparse
import os
import cv2
import numpy as np
from matplotlib import pyplot as plt

import bitplane as bp


parser = argparse.ArgumentParser(description="Image Bit-Plane Slicing")
parser.add_argument('--src', action="store", dest="im", default="../images/fractal.jpg")

args = parser.parse_args()

if not os.path.isfile(args.im):
    print('Image does not exist!')
    exit()

image = cv2.imread(args.im, cv2.IMREAD_GRAYSCALE)
bitmask_val = [2**i for i in range(8)]
plot_val = [i for i in range(241,249)]

for i in range(8):
    res = bp.bitPlane(image, i)

    plt.subplot(plot_val[i])
    plt.imshow(res, cmap='gray', interpolation='bicubic')
    plt.xticks([]), plt.yticks([])
    plt.title('bit %d' % (i))

plt.savefig('../output/bitplane.jpg')
plt.show()
