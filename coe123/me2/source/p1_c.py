"""
ME 2 Part 1: Digital Image Generation and Resolution
Task 2: Generate Gaussian Image with gray level 255
and similar to figure 3 in pdf
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

import gaussianimage as gi


side_length = 256
gl = [8, 16, 32, 64, 128, 256]
sigma = 35

im = []
plotnum = [i for i in range(231, 231 + len(gl))]  # 3-digit: nrows ncols plotnum

for i in gl:
    image = gi.generateGaussianImage(sidelength=side_length, graylevel=i, sigma=sigma)
    im.append(image)

for i in range(len(gl)):
    plt.subplot(plotnum[i])
    plt.imshow(im[i], cmap='gray')
    plt.xticks([]), plt.yticks([])
    plt.title('Gray Level %d' % (gl[i]))

plt.savefig('../output/false_contouring.jpg')
plt.show()
