"""
Bit Plane Slicing
"""

import numpy as np
import cv2

def bitPlane(image, bitnum):

    bitplane = 2**bitnum
    mask = np.full(image.shape, fill_value=bitplane, dtype=np.uint8)
    dest = cv2.bitwise_and(src1=image, src2=mask)

    return dest