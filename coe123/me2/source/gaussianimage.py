"""
Image Processing Exercise
Gaussian Image generation
Leiko Ravelo
"""

import numpy as np

# Needs optimization

def gaussian2d(x, y, xbar, ybar, sigma, k):
    xsq = (x-xbar)**2
    ysq = (y-ybar)**2
    denom = 2*3.14159*(sigma**2)

    res = k*np.exp(-1*(xsq+ysq)/denom)
    return res


def generateGaussianImage(sidelength, sigma, graylevel):
    image = np.zeros((sidelength, sidelength), dtype=np.uint8)
    xmid = sidelength/2
    ymid = sidelength/2
    if graylevel > 255: graylevel = 255

    for i in range(sidelength):
        for j in range(sidelength):
            image[i, j] = gaussian2d(i, j, xmid, ymid, sigma, graylevel)
    return image