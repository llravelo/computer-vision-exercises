## Introduction
This exercise is based from the lab exercises in the Digital Image Processing course I'm taking.
The source code requires **OpenCV 3.3.0** to run.

## Documentation
### I. Digital Images: Generation and Resolution
#### 2D Gaussian Image

A monochrome image was created using a two dimensional Gaussian function.
The Gaussian image below was produced with a *side length* of 256,
*max gray level* of 256 and a *standard deviation* of 35.  

<img src="output/gaussianimage.jpg" alt="Gaussian Image" height="150">
<br />

#### False Contouring

False contouring is a phenomenon where the false contours start to appear
in a *smooth* image region. This happens because there isn't enough grayscale
resolution to *smoothen* the transition.

<img src="output/false_contouring.jpg" alt="False Contouring" height="350">
<br />

The gray levels were adjusted while keeping other parameters constant.
It can be noticed that as the maximum gray level lowers, contours start
to appear in a supposed *smooth* gaussian image.

#### Checkerboard

Another experiment with the gaussian image was performed to observe the *checkerboard effect*.
The gaussian image was sampled at different spatial resolutions, maintaining **side length to
standard deviation ratio of 10** to remain identical in visual appearance with other images. 

<img src="output/checkerboard.jpg" alt="Checkerboard" height="350">
<br />

From the figure, checkerboard-like features start to appear at side length 128, becoming
apparent at side length 32.  

As opposed to false contouring, which results from reaching the **intensity level resolution limit**.
The checkerboard effect arises from reaching the **spatial resolution limit**.

As explained by [Jordan Ramiro et al][checkerboard]:  
>There is an interesting phenomenon known as the checkerboard effect.
>This effect is observed by leaving unchanged the number of grey levels
>and varying the spatial resolution while keeping the display area unchanged.
>The checkerboard effect is caused by pixel replication, that is,
>lower resolution images were duplicated in order to fill the display area.

### II. Basic Image Operations
#### Image Thresholding

Image thresholding works by performing thresholding intensity transformation
across the whole image. Any pixel with gray level above the threshold will be
*transorfmed* to the maximum gray level, and minimum otherwise.

<img src="output/threshold_0.jpg" alt="Low Threshold" height="250">
<img src="output/threshold_254.jpg" alt="High Threshold" height="250">
<br />

An image segmentation experiment was performed on the two cameraman images, one with light
text another with a dark one. The text was segmented with using
a threshold value of 254 and 0, respectively.

#### Bit Plane Slicing

A digital image is represented by arrays of numbers. These numbers, which contain pixel information,
are represented in the computer as **multiple bits**. In this exercise, the fractal
image data is sliced into different *bit planes* of information about the image.

<img src="images/fractal.jpg" alt="Original Image" height="350">
<img src="output/bitplane.jpg" alt="Bit Plane Slicing" height="350">
<br />

From the results shown above, it can be observed that the image represented
by bit plane 7 showed the closest resemblance with the original image. The conclusion
that can be drawn here is that the higher bits contain more information about the image
than the lower ones.

#### Green Screen

Green screen is popularly used in movies for replacing scene backgrounds. It works by casting the actors in a studio with a highly constricted color background, typically blue or green. On the computer a mask is created which filters out any blue information, retaining the actors.  

In the exercise, the image mask is created using the following python code:

```python
# Generate mask for green screen and background
# Green Threshold Default 220
ret, mask_bg = cv2.threshold(portrait[:,:,1], args.thresh, 255, cv2.THRESH_BINARY)
mask_p = cv2.bitwise_not(mask_bg)

# Merge into three channels for bitwise operation
mask_p = cv2.merge((mask_p, mask_p, mask_p))
mask_bg = cv2.merge((mask_bg, mask_bg, mask_bg))

# Mask out irrelevant portrait and background information
p = cv2.bitwise_and(portrait, mask_p)
bg = cv2.bitwise_and(background, mask_bg)

# Add two masked images
res = cv2.add(p, bg)
```

After adding the two masked images, a new image is created showing the picture of a sweet lady dancing on the beach.

<img src="output/greenscreen.jpg" alt="Green Screen" height="250">
<br />

### III. References
1. Jordan, R., Lotufo, R. (1997) . Retrieved September 22, 2017 from [this website][checkerboard]

[checkerboard]: http://www.ft.unicamp.br/docentes/magic/khoros/html-dip/c2/s10/front-page.html